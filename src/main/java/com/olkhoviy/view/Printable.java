package com.olkhoviy.view;

@FunctionalInterface
public interface Printable {
    void print();
}
