package com.olkhoviy.model;

import com.olkhoviy.model.patterns.Patterns;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toMap;

public class TextAnalyze {

    Text text;
    List<Word> words;

    public TextAnalyze(Text text) {
        this.text = text;
        words = new ArrayList<>();
    }

    private List<String> splitToWords(String txt) {
        List<String> list = new ArrayList<>();
        Pattern pattern = Patterns.WORD_PATTERN;
        Matcher matcher = pattern.matcher(txt);
        while (matcher.find()) {
            list.add(txt.substring(matcher.start(), matcher.end()));
        }

        return list.stream()
                .filter(w -> !w.isEmpty())
                .map(String::toLowerCase)
                .collect(Collectors.toList());
    }

    private List<String> getDistinct(String txt) {
        List<String> list = splitToWords(txt);
        return list.stream()
                .distinct()
                .collect(Collectors.toList());
    }

    private int countWords(String txt) {
        List<String> list = splitToWords(txt);
        return list.size();
    }

    private Map<String, Integer> countFrequencyOfWordsInText(String s) {
        Map<String, Integer> map;
        List<String> distinctWordList = getDistinct(s);
        List<String> wordList = splitToWords(s);

        map = distinctWordList.stream()
                .collect(toMap(String::toString, i -> 0));

        for (String w : wordList) {
            if (map.containsKey(w)) {
                map.put(w, map.get(w) + 1);
            }
        }
        return map;
    }

    private Map<String, Integer> getMostFrequentWord(String s) {
        Map<String, Integer> mostFrequentWord = new HashMap<>();
        Map<String, Integer> map = countFrequencyOfWordsInText(s);
        Optional<Map.Entry<String, Integer>> maxEntry = map.entrySet()
                .stream()
                .max(Map.Entry.comparingByValue());
        mostFrequentWord.put(maxEntry.get().getKey(), maxEntry.get().getValue());
        return mostFrequentWord;
    }

    public Map<String, Integer> findLargestNumberOfSentencesWithSameWords() {
        List<Sentence> list = text.getSentences();
        Map<String, Integer> mapOfAllSentences = new HashMap<>();
        Map<String, Integer> finalMap = new HashMap<>();
        for (Sentence currSentence : list) {
            mapOfAllSentences.putAll(
                    getMostFrequentWord(currSentence.getSentenceText()));
        }

        Optional<Map.Entry<String, Integer>> maxEntry = mapOfAllSentences.entrySet()
                .stream()
                .max(Map.Entry.comparingByValue());
        finalMap.put(maxEntry.get().getKey(), maxEntry.get().getValue());
        return finalMap;
    }

    public Map<Sentence, Integer> outputSentencesIncreasingWordsNumber() {
        Map<Sentence, Integer> words = new HashMap<>();
        List<Sentence> listOfSentences = text.getSentences();
        for (Sentence s : listOfSentences) {
            words.put(s, countWords(s.getSentenceText()));
        }
        Map<Sentence, Integer> sorted = words.entrySet().stream()
                .sorted(Map.Entry.comparingByValue())
                .collect(toMap(e -> e.getKey(), e -> e.getValue(),
                        (e1, e2) -> e2, LinkedHashMap::new));
        return sorted;
    }

    private boolean isQuestionSentence(Sentence s) {
        Pattern pattern = Patterns.QUESTION_PATTERN;
        Matcher matcher = pattern.matcher(s.getSentenceText());
        return matcher.find();
    }

    private List<Sentence> showQuestionSentences() {
        List<Sentence> questionList = new ArrayList<>();
        List<Sentence> fullList = text.getSentences();
        for (Sentence s : fullList) {
            if (isQuestionSentence(s)) {

                questionList.add(s);
            }
        }
        return questionList;
    }

    private List<Word> findWordsOfGivenLength(int length, Sentence s) {
        List<String> list = getDistinct(s.getSentenceText());
        List<Word> wordsOfGivenLength = new ArrayList<>();
        for (String str : list) {
            if (str.length() == length) {
                wordsOfGivenLength.add(new Word(str));
            }
        }
        return wordsOfGivenLength;
    }

    public List<Word> outputQuestionWordsOfGivenLength(int length) {
        List<Word> questionWordsOfGivenLength = new ArrayList<>();
        List<Sentence> questionSentenceList = showQuestionSentences();
        for (Sentence s : questionSentenceList) {
            List<Word> curr = findWordsOfGivenLength(length, s);
            questionWordsOfGivenLength.addAll(curr);
        }
        return questionWordsOfGivenLength;
    }

    public List<String> printoutSortedWords() {
        List<String> words = splitToWords(text.getInputText());
        List<String> sortedWords = words.stream()
                .distinct()
                .sorted()
                .collect(Collectors.toList());
        return sortedWords;
    }

    private double countPercentageOfVowelLetter(Word w) {
        double numberOfVowels = w.getWord()
                .replaceAll("[^aeiouAEIOU]", "")
                .length();
        return (numberOfVowels * 100) / w.getWord().length();
    }

    public Map<Word, Double> getByPercentageOfVowelLetter() {
        List<String> strings = getDistinct(text.getInputText());
        List<Word> wordList = strings.stream()
                .map(Word::new)
                .collect(Collectors.toList());
        Map<Word, Double> percentageMap = new HashMap<>();
        for (Word w : wordList) {
            Double percentage = countPercentageOfVowelLetter(w);
            percentageMap.put(w, percentage);
        }

        return percentageMap;
    }

}
