package com.olkhoviy.model;

import java.util.List;

public class Sentence {

    List<Word> words;
    private String sentenceText;

    public Sentence(String sentenceText) {
        this.sentenceText = sentenceText;
    }

    public String getSentenceText() {
        return sentenceText;
    }

    public List<Word> getWords() {
        return words;
    }

    @Override
    public String toString() {
        return "Sentence{" + sentenceText + '}' + '\n';
    }

}
