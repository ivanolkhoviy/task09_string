package com.olkhoviy.controller;
import com.olkhoviy.model.Sentence;
import com.olkhoviy.model.Word;

import java.util.List;
import java.util.Map;

public interface Controller {

    Map<String, Integer> doTask1();

    Map<Sentence, Integer> doTask2();

    void doTask3();

    List<Word> doTask4(int length);

    void doTask5();

    List<String> doTask6();

    Map<Word, Double> doTask7();
}
